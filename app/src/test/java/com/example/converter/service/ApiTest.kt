package com.example.converter.service

import com.squareup.moshi.Moshi
import io.reactivex.subscribers.TestSubscriber
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.math.BigDecimal


class ApiTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: Api
    private val testSubscriber = TestSubscriber.create<Map<CurrencyIsoCode, BigDecimal>>()

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        val moshi = Moshi.Builder()
            .add(CurrencyAdapter())
            .build()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(ResponseConverter())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(Api::class.java)
    }

    @Test
    fun verifyRequest() {
        enqueueResponse("latest.json")

        service.getRates()
            .toFlowable()
            .subscribe(testSubscriber)

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/latest?base=EUR"))
    }

    @Test
    fun verifyResponse() {
        enqueueResponse("latest.json")

        service.getRates()
            .toFlowable()
            .subscribe(testSubscriber)

        testSubscriber.assertNoErrors()
        testSubscriber.assertValueCount(1)
        assertEquals(testSubscriber.values()[0].size, 33)
    }

    private fun enqueueResponse(fileName: String) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        mockWebServer.enqueue(mockResponse.setBody(source.readUtf8()))
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}