package com.example.converter.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.example.converter.main.Resource.Success
import com.example.converter.service.Api
import com.example.converter.service.CurrencyAdapter
import com.example.converter.service.CurrencyIsoCode
import com.example.converter.service.CurrencyIsoCode.AUD
import com.example.converter.service.CurrencyIsoCode.EUR
import com.example.converter.service.Listing
import com.example.converter.service.Response
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.Moshi
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert.assertEquals
import okio.Okio
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.SECONDS

class MainViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: MainViewModel
    private val testScheduler = TestScheduler()

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        RxJavaPlugins.setSingleSchedulerHandler { testScheduler }
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { testScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { testScheduler }
    }

    @Test
    fun loadData() {
        viewModel = MainViewModel(FakeApi())
        val observer: Observer<Resource> = mock()
        viewModel.result.observeForever(observer)

        testScheduler.triggerActions()

        verify(observer).onChanged(any<Resource.Success<*>>())
    }

    @Test
    fun loadDataRepeats() {
        val api = mock<FakeApi> {
            whenever(it.getRates()).thenCallRealMethod()
        }
        viewModel = MainViewModel(api)
        val observer: Observer<Resource> = mock()

        viewModel.result.observeForever(observer)

        testScheduler.advanceTimeBy(1, SECONDS)
        verify(api).getRates()
        verify(observer, times(2)).onChanged(any<Resource.Success<*>>())

        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        verify(api).getRates()
        verify(observer, times(3)).onChanged(any<Resource.Success<*>>())

        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        verify(api).getRates()
        verify(observer, times(4)).onChanged(any<Resource.Success<*>>())
    }

    @Test
    fun loadDataError() {
        val api = mock<ErrorApi> {
            whenever(it.getRates()).thenCallRealMethod()
        }
        viewModel = MainViewModel(api)
        val observer: Observer<Resource> = mock()

        viewModel.result.observeForever(observer)
        testScheduler.triggerActions()

        verify(api).getRates()
        verify(observer).onChanged(any<Resource.Error>())
        verifyNoMoreInteractions(api)
    }

    @Suppress("UNCHECKED_CAST")
    @Test
    fun checkDivision() {
        viewModel = MainViewModel(FakeApi())
        val observer: Observer<Resource> = mock()
        viewModel.changeBase(AUD, BigDecimal(100))

        viewModel.result.observeForever(observer)
        testScheduler.triggerActions()

        val resource: Resource.Success<List<Listing>> = viewModel.result.value as Success<List<Listing>>
        val list = resource.value
        assertEquals(list[0].currency, AUD)
        assertEquals(list[0].rate.intValueExact(), 100)
        assertEquals(list.find { it.currency == EUR }!!.rate.intValueExact(), 50)
    }

    @Test
    fun retry() {
        val api = mock<ErrorApi> {
            whenever(it.getRates()).thenCallRealMethod()
        }
        viewModel = MainViewModel(api)
        val observer: Observer<Resource> = mock()

        viewModel.result.observeForever(observer)
        testScheduler.triggerActions()

        verify(api).getRates()
        verify(observer).onChanged(any<Resource.Error>())

        viewModel.retry()

        verify(api, times(2)).getRates()
        verify(observer).onChanged(any<Resource.Error>())
    }

    open class FakeApi : Api {
        override fun getRates(base: CurrencyIsoCode): Single<Map<CurrencyIsoCode, BigDecimal>> {
            val moshi = Moshi.Builder()
                .add(CurrencyAdapter())
                .build()
            val inputStream = javaClass.classLoader
                .getResourceAsStream("api-response/latest.json")
            val source = Okio.buffer(Okio.source(inputStream))
            val adapter = moshi.adapter(Response::class.java)
            val response = adapter.fromJson(source.readUtf8())
            return Single.just(response!!.rates)
        }
    }

    open class ErrorApi : Api {
        override fun getRates(base: CurrencyIsoCode): Single<Map<CurrencyIsoCode, BigDecimal>> {
            return Single.error(Exception("wtf"))
        }
    }
}
