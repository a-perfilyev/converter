package com.example.converter.di

import com.example.converter.service.Api
import com.example.converter.service.CurrencyIsoCode
import io.reactivex.Single
import java.math.BigDecimal

class TestComponent : AppComponent {
    override val api: Api
        get() = FakeApi()
}

class FakeApi : Api {
    override fun getRates(base: CurrencyIsoCode): Single<Map<CurrencyIsoCode, BigDecimal>> {
        val aud = CurrencyIsoCode.AUD to BigDecimal.valueOf(3)
        val cad = CurrencyIsoCode.CAD to BigDecimal.valueOf(5)
        val eur = CurrencyIsoCode.EUR to BigDecimal.valueOf(1)
        val gbp = CurrencyIsoCode.GBP to BigDecimal.valueOf(4)
        val usd = CurrencyIsoCode.USD to BigDecimal.valueOf(2)
        return Single.just(mapOf(eur, usd, aud, gbp, cad))
    }
}