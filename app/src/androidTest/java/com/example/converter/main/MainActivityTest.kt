package com.example.converter.main

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.example.converter.R
import com.example.converter.util.RecyclerViewMatcher
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun loadResults() {
        val recyclerView = rule.activity.findViewById<RecyclerView>(R.id.recycler_view)
        assertEquals(5, recyclerView.adapter.itemCount)
        onView(listMatcher().atPosition(0))
            .check(matches(hasDescendant(withText("EUR"))))
        onView(withId(android.support.design.R.id.snackbar_text)).check(doesNotExist())
    }


    @Test
    fun clickAtNonZeroChangesSorting() {
        onView(withId(R.id.recycler_view))
            .perform(actionOnItemAtPosition<MainAdapter.MainViewHolder>(2, click()))
        onView(listMatcher().atPosition(0))
            .check(matches(hasDescendant(withText("CAD"))))
    }

    @Test
    fun clickAtZeroDoesntChangeSorting() {
        onView(withId(R.id.recycler_view))
            .perform(actionOnItemAtPosition<MainAdapter.MainViewHolder>(0, click()))
        onView(listMatcher().atPosition(0))
            .check(matches(hasDescendant(withText("EUR"))))
    }

    private fun listMatcher(): RecyclerViewMatcher {
        return RecyclerViewMatcher(R.id.recycler_view)
    }
}
