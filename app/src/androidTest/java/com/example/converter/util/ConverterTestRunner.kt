package com.example.converter.util

import android.app.Application
import android.content.Context
import android.support.test.runner.AndroidJUnitRunner
import com.example.converter.TestApp

class ConverterTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, TestApp::class.java.name, context)
    }
}