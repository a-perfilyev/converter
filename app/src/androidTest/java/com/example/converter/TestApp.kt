package com.example.converter

import com.example.converter.di.AppComponent
import com.example.converter.di.TestComponent

class TestApp : App() {
    override val appComponent: AppComponent by lazy {
        TestComponent()
    }
}