package com.example.converter

import android.app.Application
import com.example.converter.di.AppComponent
import com.example.converter.di.NetModule
import com.example.converter.di.NetModuleImpl
import timber.log.Timber

open class App : Application() {

    // no need to use dagger or another di framework for such a small app
    open val appComponent: AppComponent by lazy {
        object : AppComponent, NetModule by NetModuleImpl() {}
    }

    override fun onCreate() {
        super.onCreate()
        if (!BuildConfig.DEBUG) return

        Timber.plant(Timber.DebugTree())
    }
}