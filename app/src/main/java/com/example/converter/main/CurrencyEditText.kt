package com.example.converter.main

import android.content.Context
import android.support.v7.appcompat.R
import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.util.AttributeSet
import java.math.BigDecimal
import java.math.RoundingMode.HALF_UP
import java.text.DecimalFormat
import java.text.NumberFormat

class CurrencyEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = R.attr.editTextStyle
) : AppCompatEditText(context, attrs, defStyleAttr) {

    private var listener: ((BigDecimal) -> Unit)? = null
    private var rawValue: BigDecimal = BigDecimal.ZERO
    private val textWatcher = CurrencyFormattingTextWatcher()

    fun setListener(listener: (BigDecimal) -> Unit) {
        this.listener = listener
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addTextChangedListener(textWatcher)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeTextChangedListener(textWatcher)
        listener = null
    }

    fun setValue(value: BigDecimal) {
        if (rawValue != value) {
            rawValue = value
            setText(value.format())
        }
    }

    fun getValue() = rawValue

    /**
     * Good enough implementation for a test task.
     */
    private inner class CurrencyFormattingTextWatcher : TextWatcher {

        private var selfChange = false
        private val parser = BigDecimalParser

        override fun afterTextChanged(s: Editable) {
            if (!isEnabled || selfChange) return

            val digits = s.toString().filter(Char::isDigit)
            rawValue = parser.parse(digits)
                .divide(BigDecimal(100), 4, HALF_UP)

            val formatted = rawValue.format()
            selfChange = true
            listener?.invoke(rawValue)
            setText(formatted)
            setSelection(maxOf(0, formatted.length))
            selfChange = false
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        }
    }
}

fun BigDecimal.format(): String {
    val numberFormat = NumberFormat.getCurrencyInstance()
    val symbols = (numberFormat as DecimalFormat).decimalFormatSymbols
    symbols.currencySymbol = ""
    numberFormat.decimalFormatSymbols = symbols
    return numberFormat.format(toDouble())
}