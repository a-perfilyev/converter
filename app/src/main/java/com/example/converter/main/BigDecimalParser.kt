package com.example.converter.main

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

object BigDecimalParser {
    private val format: DecimalFormat by lazy(LazyThreadSafetyMode.NONE) {
        DecimalFormat().apply {
            isParseBigDecimal = true
        }
    }

    fun parse(string: String): BigDecimal {
        if (string.isEmpty()) return BigDecimal.ZERO

        val parsed = format.parse(string)
        return BigDecimal(parsed.toDouble())
            .setScale(4, RoundingMode.HALF_UP)
    }
}