package com.example.converter.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.converter.service.Api
import com.example.converter.service.CurrencyIsoCode
import com.example.converter.service.Listing
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import org.reactivestreams.Publisher
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit

class MainViewModel(private val api: Api) : ViewModel() {

    private val base = BehaviorProcessor.createDefault(CurrencyIsoCode.EUR).toSerialized()
    private val amount = BehaviorProcessor.createDefault(BigDecimal(100)).toSerialized()
    private val trigger = PublishRelay.create<Unit>()
    val result: LiveData<Resource>

    init {
        result = trigger.toFlowable(BackpressureStrategy.BUFFER)
            .startWith(Unit)
            .switchMap { makeNetworkCall() }
            .observeOn(mainThread())
            .toLiveData()
    }

    private fun makeNetworkCall(): Flowable<Resource> {
        return api.getRates()
            .repeatWhen {
                it.delay(1, TimeUnit.SECONDS)
            }
            .flatMap(::createSortedListFlowable)
            .map<Resource> { Resource.Success(it) }
            .onErrorReturn { Resource.Error(it.localizedMessage) }
    }

    private fun createSortedListFlowable(ratesMap: Map<CurrencyIsoCode, BigDecimal>): Flowable<List<Listing>> {
        val ratesList = ratesMap.map { (c, r) ->
            Listing(currency = c, rate = r)
        }
        return Flowable.combineLatest(base.take(1), amount.take(1), Combiner { b, a ->
            val baseRate = ratesMap.getValue(b)
            ratesList.sortedWith(compareByDescending<Listing> { it.currency == b }.thenBy { it.currency })
                .map {
                    it.copy(
                        rate = calculateCrossRate(it.rate, baseRate, a).setScale(
                            4,
                            RoundingMode.HALF_UP
                        )
                    )
                }
        })
    }

    private fun calculateCrossRate(
        rate: BigDecimal,
        baseRate: BigDecimal,
        amount: BigDecimal
    ): BigDecimal {
        return rate.divide(baseRate, 4, RoundingMode.HALF_UP).multiply(amount)
    }

    fun changeBase(newCurrency: CurrencyIsoCode, newAmount: BigDecimal) {
        base.onNext(newCurrency)
        amount.onNext(newAmount)
        trigger.accept(Unit)
    }

    fun retry() {
        trigger.accept(Unit)
    }

    class Factory(private val api: Api) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MainViewModel(api) as T
        }
    }
}

sealed class Resource {
    data class Success<T>(val value: T) : Resource()
    data class Error(val error: String) : Resource()
}

private fun <T> Publisher<T>.toLiveData(): LiveData<T> {
    return LiveDataReactiveStreams.fromPublisher(this)
}

typealias Combiner = BiFunction<CurrencyIsoCode, BigDecimal, List<Listing>>
