package com.example.converter.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.converter.App
import com.example.converter.R
import com.example.converter.service.Listing
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val appComponent by lazy {
        (application as App).appComponent
    }

    private val factory by lazy {
        MainViewModel.Factory(appComponent.api)
    }

    private lateinit var vm: MainViewModel

    private var shouldScrollToTop = false
    private val adapter = MainAdapter { c, a ->
        vm.changeBase(c, a)
        shouldScrollToTop = true
    }

    private val observer = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            if (toPosition == 0) adapter.notifyItemChanged(0)

            if (shouldScrollToTop) {
                (recycler_view.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
                    0,
                    0
                )
                shouldScrollToTop = false
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter.registerAdapterDataObserver(observer)
        recycler_view.adapter = adapter
        vm = get(factory)
        vm.result.observe(this, Observer {
            @Suppress("UNCHECKED_CAST")
            when (it) {
                is Resource.Success<*> -> adapter.submitList(it.value as List<Listing>)
                is Resource.Error -> showSnackbar()
            }

        })
    }

    private fun showSnackbar() {
        Snackbar.make(recycler_view, "An error has occurred", Snackbar.LENGTH_INDEFINITE)
            .setAction("Retry") { vm.retry() }
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter.unregisterAdapterDataObserver(observer)
    }
}

inline fun <reified VM : ViewModel> FragmentActivity.get(factory: ViewModelProvider.Factory): VM {
    return ViewModelProviders.of(this, factory).get(VM::class.java)
}