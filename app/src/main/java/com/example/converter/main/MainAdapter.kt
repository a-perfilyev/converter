package com.example.converter.main

import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.converter.R
import com.example.converter.main.MainAdapter.MainViewHolder
import com.example.converter.service.CurrencyIsoCode
import com.example.converter.service.Listing
import kotlinx.android.synthetic.main.item_currency.view.*
import java.math.BigDecimal
import java.util.*

class MainAdapter(private val clickListener: ClickListener) :
    ListAdapter<Listing, MainViewHolder>(DiffCallback()) {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val viewHolder = MainViewHolder.create(parent) { currency, amount ->
            clickListener(currency, amount)
        }
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            if (position > 0) {
                val (currency) = getItem(position)
                val amount = viewHolder.itemView.currency_value.getValue()
                clickListener(currency, amount)
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }

    override fun getItemId(position: Int): Long {
        val item = getItem(position)
        return item.currency.ordinal.toLong()
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(
        holder: MainViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        holder.bindItem(getItem(position), payloads.firstOrNull() as Pair<Listing, Listing>?)
    }

    class MainViewHolder(
        itemView: View,
        private val callback: (CurrencyIsoCode, BigDecimal) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        private val code: TextView = itemView.currency_code
        private val name: TextView = itemView.currency_name
        private val value: CurrencyEditText = itemView.currency_value

        fun bindItem(listing: Listing, payload: Pair<Listing, Listing>?) {
            val currency = Currency.getInstance(listing.currency.name)
            code.text = currency.currencyCode
            name.text = currency.displayName
            payload?.takeUnless { adapterPosition == 0 }?.let(::animateChange) ?: run {
                value.setValue(listing.rate)
            }
            value.isEnabled = adapterPosition == 0
            value.setListener {
                callback(CurrencyIsoCode.valueOf(code.text.toString()), it)
            }
        }

        private fun animateChange(payload: Pair<Listing, Listing>) {
            val (old, new) = payload
            with(ValueAnimator.ofObject(BigDecimalEvaluator, old.rate, new.rate)) {
                duration = 250
                addUpdateListener {
                    value.setValue(it.animatedValue as BigDecimal)
                }
                start()
            }
        }

        companion object {
            fun create(
                parent: ViewGroup,
                callback: (CurrencyIsoCode, BigDecimal) -> Unit
            ): MainViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val itemView = layoutInflater.inflate(R.layout.item_currency, parent, false)
                return MainViewHolder(itemView, callback)
            }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Listing>() {
        override fun areItemsTheSame(oldItem: Listing, newItem: Listing): Boolean {
            return oldItem.currency == newItem.currency
        }

        override fun areContentsTheSame(oldItem: Listing, newItem: Listing): Boolean {
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: Listing, newItem: Listing): Any {
            return oldItem to newItem
        }
    }
}

typealias ClickListener = (CurrencyIsoCode, BigDecimal) -> Unit

object BigDecimalEvaluator : TypeEvaluator<BigDecimal> {
    override fun evaluate(
        fraction: Float,
        startValue: BigDecimal,
        endValue: BigDecimal
    ): BigDecimal {
        val minus = endValue.minus(startValue)
        val multiply = minus.multiply(BigDecimal(fraction.toDouble()))
        return startValue.add(multiply)
    }
}