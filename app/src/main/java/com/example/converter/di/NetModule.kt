package com.example.converter.di

import com.example.converter.service.Api
import com.example.converter.service.CurrencyAdapter
import com.example.converter.service.ResponseConverter
import com.squareup.moshi.Moshi
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

interface NetModule {
    val api: Api
}

const val BASE_URL = "https://revolut.duckdns.org"

class NetModuleImpl : NetModule {
    override val api: Api by lazy {
        val moshi = Moshi.Builder()
            .add(CurrencyAdapter())
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ResponseConverter())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

        retrofit.create(Api::class.java)
    }
}