package com.example.converter.service

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal

class CurrencyAdapter {

    @FromJson
    fun fromJson(json: String): BigDecimal {
        return BigDecimal(json)
    }

    @ToJson
    fun toJson(bigDecimal: BigDecimal): String {
        return bigDecimal.toString()
    }
}