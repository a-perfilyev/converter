package com.example.converter.service

import com.example.converter.service.CurrencyIsoCode.EUR
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal

const val PATH = "latest"
const val QUERY = "base"

interface Api {

    @GET(PATH)
    fun getRates(@Query(QUERY) base: CurrencyIsoCode = EUR): Single<Map<CurrencyIsoCode, BigDecimal>>
}