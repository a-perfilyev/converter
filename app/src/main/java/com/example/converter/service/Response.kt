package com.example.converter.service

import java.math.BigDecimal

data class Response(val rates: Map<CurrencyIsoCode, BigDecimal>)
data class Listing(val currency: CurrencyIsoCode, val rate: BigDecimal)
enum class CurrencyIsoCode {
    AUD, BGN, BRL,
    CAD, CHF, CNY,
    CZK, DKK, EUR,
    GBP, HKD, HRK,
    HUF, IDR, ILS,
    INR, ISK, JPY,
    KRW, MXN, MYR,
    NOK, NZD, PHP,
    PLN, RON, RUB,
    SEK, SGD, THB,
    TRY, USD, ZAR
}