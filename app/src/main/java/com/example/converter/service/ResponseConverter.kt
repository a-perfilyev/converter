package com.example.converter.service

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.math.BigDecimal

class ResponseConverter : Converter.Factory() {
    override fun responseBodyConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        val delegate =
            retrofit.nextResponseBodyConverter<Response>(this, Response::class.java, annotations)
        return Converter<ResponseBody, Any> {
            val response = delegate.convert(it)
            response.rates.plus(CurrencyIsoCode.EUR to BigDecimal.ONE)
        }
    }
}